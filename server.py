__author__ = 'U552284'
from flask import Flask, render_template, make_response
import json
import random

app = Flask(__name__)
# Przykładowy plik z danymi
AIRPORT_DATA = "data/London, United Kingdom Heathrow airport.json"

airport_data = json.load(open(AIRPORT_DATA, "rt", encoding="UTF-8"))
print(len(airport_data))

@app.route("/airports/<int:num>")
def get_airports(num):
    data = json.dumps([random.choice(airport_data) for i in range(num)])
    response = make_response(data)
    response.headers['Access-Control-Allow-Origin'] = ['*']
    return response
    #photos = [random.choice(airport_data) for i in range(num)]
    #return render_template("carousel.html", photos=photos)

if __name__ == "__main__":
    #app.run(host='10.232.45.237')
    app.run() # localhost