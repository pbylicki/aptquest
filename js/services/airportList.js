/**
 * Created by PawelBylicki on 2015-11-02.
 */
app.factory('airportList', ['$http', function($http) {
    //put here path to json with photo data
    /*return $http.get('js/airports.json')
        .success(function(data) {
            return data;
        })
        .error(function(err) {
            return err;
        });*/
    return {
        getPhotos: function(fileName){
            return $http.get('data/' + fileName)
                .success(function(data) {
                    return data;
                })
                .error(function(err) {
                    return err;
                });
        },
        getPlanes: function(fileName){
            return $http.get('aircrafts_db/' + fileName)
                .success(function(data) {
                    return data;
                })
                .error(function(err) {
                    return err;
                });
        }
    };
}]);