app.controller('PlaneController', ['$scope', '$http', 'airportList', function($scope, $http, airportList) {
    $scope.aircraftFile = "";
    $scope.verify = function(index){
        if($scope.photos[index]["verified"]) return;
        $http.post('https://df-aptchallenge.enterprise.dreamfactory.com:443/api/v2/db/_table/plane',
        $scope.photos[index],
            {headers: {'X-DreamFactory-Api-Key': 'c52e9264e7172983fa3ba5d7cc0c32d5ba23c995f4a3ef5158ddcbab1e2322f3'}})
            .success(function(data){
                $scope.photos[index]["verified"] = "Verified"
            })
            .error(function(error){
                $scope.photos[index]["verified"] = "Error occurred"
            })
    };
    $scope.openFile = function(){
        if(!$scope.aircraftFile) return;
        airportList.getPlanes($scope.aircraftFile)
            .success(function(data){
                $scope.photos = data;
            })
            .error(function(error){
                alert(error);
            });
    }
}]);